/**************************************************************************************************/
/* Author: Andrew Barndt                                                                          */
/* Description: A maze generator that uses recursion to create mazes either randomly or with some */
/* certain conditions, such as having a point in the maze the solution must run through. Sometimes*/
/* produces mazes with solutions not running through that point, but fairly infrequently. After   */
/* generating the map, creates a bitmap image of the maze. Memory is dynamically allocated, and   */
/* freed after use.                                                                               */
/*                                                                                                */
/* How to use: this program runs with a simple gcc command, with no extra libraries needed. All   */
/* images used as tiles must be in the current directory, and must have dimensions of 12x12 pixels*/
/* (images are provided in the submission in Learn). Otherwise, there's nothing special about     */
/* running this generator.                                                                        */
/**************************************************************************************************/

#include "mazegen.h"
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>

//Struct holding the info for the end of each "arm" of the maze that originates from the alleys, and
//holds the starting location of the end of the original alleys
struct WayPointEnd
{
    int x; int y; char direction; int xStart; int yStart;
};

//Initialize the structs with variables that get changed later 
struct WayPointEnd w1 = {0, 0, 0, 0, 0};
struct WayPointEnd w2 = {0, 0, 0, 0, 0};
struct WayPointEnd w3 = {0, 0, 0, 0, 0};
struct WayPointEnd w4 = {0, 0, 0, 0, 0};

//function prototypes
void carveMaze(int x, int y, int dir, char wayPoint);
void shuffle(char *arr);
void solutionSearch(int x, int y, int dir);
void determinePath(struct WayPointEnd *w, int wayPoint);
void newLocation(struct WayPointEnd *w, int wayPoint);
void ifTop (int x, int y);
void ifBottom (int x, int y);
void setRGB(unsigned char data[], int x, int y, int rowSize, int pixelHeight,
            unsigned char r, unsigned char g, unsigned char b);
void copyIntToAddress(int n, unsigned char bytes[]);
void makeImage();
void processImage(unsigned char img[], int x, int y, int rowSize, int pixelHeight, char name[]);

static unsigned char **maze;
int hitBottom, hitTop; //to keep track of if the maze has already reached the maze's bottom or top to determine
                       //if the maze needs to blast a hole through either
int ifFound; //used by the solver to keep track of if the way through has been found
int hasBeenFreed = 0; //keeps track of memory freeing
int top, bottom, carvesMade;

//Global variables to hold info passed into mazeGenerate
int mazeHeight, mazeWidth;
int xWayPoint, yWayPoint;
int totalCarve, carvesMade;
int straightPercentage, printAtStep;

/**************************************************************************************************/
/* width and height: can take values from 3 to 1000 to set the maze's dimensions. wayPointX and   */
/* wayPointY are the coordinates of the waypoint, which can take any value within the maze.       */
/* wayPointAlleyLength is the length of an alley from the waypoint, which could be up to the      */
/* height or width of the maze. wayPointDirectionPercent is a number between 0 and 1 indicating   */
/* the number of cells that can be carved at one time from the end of one of the "arms"; a 0 means*/
/* no cells can be carved, while 1 means the entire maze can be carved from one point. The        */
/* straightProbability parameter also can be anywhere from 0 to 1, with 0.5 meaning there's about */
/* a 50% chance the maze continues in the same direction it was going before, 0 meaning it chooses*/
/* a random direction and 1 meaning it continues in the same direction until it hits an edge or   */
/* another part of the maze. printAlgorithmSteps controls if the maze gets printed each time a new*/
/* part is carved, as well as when the end of carveMaze for each cell is reached and returns.     */
/*                                                                                                */
/* The function starts by freeing any old maze memory, then performs error checking on parameters.*/
/* After initializing variables for the maze and allocating memory, the edges are marked off to   */
/* make bounds checking much easier. The waypoint alley is then created, with checking for edges  */
/* if the waypoint can't go as far as indicated. The maze then sends carveMaze a starting point   */
/* with a random direction.                                                                       */
/**************************************************************************************************/

int mazeGenerate(int width, int height, int wayPointX, int wayPointY, int wayPointAlleyLength,
    double wayPointDirectionPercent,  double straightProbability, int printAlgorithmSteps)
{
    mazeFree();

    //Error check each parameter
    if (height < 3 || height > 1000 || width < 3 || width > 1000)
    {
        printf("Invalid dimensions\n");
        return TRUE;
    }
    if (wayPointX < 1 || wayPointX > width || wayPointY < 1 || wayPointY > height)
    {
        printf("Invalid waypoints\n");
        return TRUE;
    }
    if (((width > height) && (wayPointAlleyLength > height/2)) || wayPointAlleyLength < 0 ||
        ((height > width) && (wayPointAlleyLength > width/2)))
    {
        printf("Invalid alley length\n");
        return TRUE;
    }
    if (wayPointDirectionPercent < 0.0 || wayPointDirectionPercent > 1.0)
    {
        printf("Invalid percentages\n");
        return TRUE;
    }
    if (straightProbability < 0.0 || straightProbability > 1.0)
    {
        printf("Invalid probability\n");
        return TRUE;
    }
    if (printAlgorithmSteps > 1 || printAlgorithmSteps < 0)
    {
        printf("Invalid algorithm step input\n");
        return TRUE;
    }

    //Variable initialization
    mazeHeight = height + 2; mazeWidth = width + 2;
    hitTop = FALSE; hitBottom = FALSE; ifFound = FALSE;
    printAtStep = printAlgorithmSteps;
    totalCarve = mazeWidth * mazeHeight * wayPointDirectionPercent;
    carvesMade = 0;
    yWayPoint = wayPointY; xWayPoint = wayPointX;

    //Allocate memory for maze
    maze = malloc(mazeHeight * sizeof(char *));
    for (int i = 0; i < mazeHeight; i++)
    { maze[i] = malloc(mazeWidth * sizeof(char));
    }

    for (int i = 1; i < mazeWidth - 1; i++)
    {
        for (int j = 1; j < mazeHeight - 1; j++)
        {
            maze[j][i] = 0;
        }
    }
    hasBeenFreed = FALSE;

    //Give the boundary of the maze values of 15 to ensure they're never blasted into
    for (int i = 0; i < mazeWidth; i++) maze[0][i] = ALL_DIRECTIONS;
    for (int i = 0; i < mazeWidth; i++) maze[mazeHeight - 1][i] = ALL_DIRECTIONS;
    for (int i = 0; i < mazeHeight; i++) maze[i][0] = ALL_DIRECTIONS;
    for (int i = 0; i < mazeHeight; i++) maze[i][mazeWidth - 1] = ALL_DIRECTIONS;
    if (wayPointAlleyLength > 0) maze[wayPointY][wayPointX] = ALL_DIRECTIONS;    
    
    maze[wayPointY][wayPointX] |= SPECIAL;
    //Set the waypoints and mark their x and y coordinate endpoints

    //These variables are changed if the alley length is greater than 0, but initialized here in case
    //the alley length is 0
    w1.y = wayPointY; w2.x = wayPointX; w3.y = wayPointY; w4.x = wayPointX;
    
    //Basic algorithm: loop through each spot in the maze that the waypoint goes through and start by
    //making it go halfway through the current coordinate; if it's allowed to go into the next coordinate,
    //then it goes all the way through the current coordinate and the loop iterates over that coordinate.
    
    //Alley going east
    for (int i = wayPointX + 1; i <= wayPointX + wayPointAlleyLength; i++) 
    {
        if (i - 1 >= wayPointX) maze[wayPointY][i] |= (SPECIAL | WEST);
        if (maze[wayPointY][i + 1] == ALL_DIRECTIONS) 
        {
            w2.x = i;
            break;
        }
        if (i + 1 <= wayPointX + wayPointAlleyLength) maze[wayPointY][i] |= (SPECIAL | EAST);
        w2.x = i;
    }

    //Alley going west
    for (int i = wayPointX - 1; i >= wayPointX - wayPointAlleyLength; i--)
    {
        if (i + 1 <= wayPointX) maze[wayPointY][i] |= (SPECIAL | EAST);
        if (maze[wayPointY][i - 1] == ALL_DIRECTIONS)
        {
            w4.x = i;
            break;
        }
        if (i - 1 >= wayPointX - wayPointAlleyLength) maze[wayPointY][i] |= (SPECIAL | WEST);
        w4.x = i;
    }

    //Alley going south
    for (int i = wayPointY + 1; i <= wayPointY + wayPointAlleyLength; i++)
    {
        if (i - 1 >= wayPointY) maze[i][wayPointX] |= (SPECIAL | NORTH);
        if (maze[i + 1][wayPointX] == ALL_DIRECTIONS)
        {
            w3.y = i;
            break;
        }
        if (i + 1 <= wayPointY + wayPointAlleyLength) maze[i][wayPointX] |= (SPECIAL | SOUTH);
        w3.y = i;
    }

    //Alley going north
    for (int i = wayPointY - 1; i >= wayPointY - wayPointAlleyLength; i--)
    {
        if (i + 1 <= wayPointY) maze[i][wayPointX] |= (SPECIAL | SOUTH);
        if (maze[i - 1][wayPointX] == ALL_DIRECTIONS)
        {
            w1.y = i;
            break;
        }
        if (i - 1 >= wayPointY - wayPointAlleyLength) maze[i][wayPointX] |= (SPECIAL | NORTH);
        w1.y = i;
    }
    
    //Assign remaining x and y coordinates
    w1.x = wayPointX; w2.y = wayPointY; w3.x = wayPointX; w4.y = wayPointY;

    //Hold the starting points in the struct
    w1.yStart = w1.y; w1.xStart = w1.x;
    w2.yStart = w2.y; w2.xStart = w2.x;
    w3.yStart = w3.y; w3.xStart = w3.x;
    w4.yStart = w4.y; w4.xStart = w4.x;

    w1.direction = SOUTH; w2.direction = WEST; w3.direction = NORTH; w4.direction = EAST;

    if (printAtStep) mazePrint();

    //Randomly choose starting point for maze and start carving it
    straightPercentage = straightProbability * 100;
    char randomDir = DIRECTION_LIST[rand() % 4];
    switch(randomDir)
    {
        case NORTH: carveMaze(w1.x, w1.y, SOUTH, 1); break;
        case EAST:  carveMaze(w2.x, w2.y, WEST, 2); break;
        case SOUTH: carveMaze(w3.x, w3.y, NORTH, 3); break;
        case WEST:  carveMaze(w4.x, w4.y, EAST, 4); break;
    }
    makeImage();
    return FALSE;
}

/**************************************************************************************************/
/* int x and y represent the maze coordinates. The function is used to determine if the ending    */
/* point for the maze has been found, which is only true if the bottom hasn't been found yet and  */
/* the maze is either at the corner or within 1/5 of the total width of the maze from the corner. */
/* The function does not check that y is equal to mazeHeight - 2 (which would be on the bottom of */
/* the maze) as that is checked before the function is called. Lastly, it sets the carvesMade     */
/* variable equal to the value it must be to cause the maze to carve from a different location.   */
/**************************************************************************************************/

void ifBottom (int x, int y)
{
    if (hitBottom == FALSE  && (x == 1 || x == mazeWidth - 2 || x < mazeWidth/5 || x > (4 * mazeWidth)/5))
    {
        hitBottom = TRUE;
        bottom = x;
        maze[y][x] |= SOUTH;
        carvesMade = totalCarve + 1;
    }
}

/**************************************************************************************************/
/* Very similar function to ifBottom. Is only called when y = 1 (indicating the coordinates are on*/
/* top of the maze. x and y are used as coordinates, then if the top (starting point) of the maze */
/* is to be at this location, the start must not already have been found, and the maze is either  */
/* at the corner or within 1/5 of the total maze width of the corner. This function too sets the  */
/* carvesMade variable equal to the number that causes the maze the carve from a new location.    */
/**************************************************************************************************/

void ifTop (int x, int y)
{
    if (hitTop == FALSE && (x == 1 || x == mazeWidth - 2 || x < mazeWidth/5 || x > (4 * mazeWidth)/5))
    {
        hitTop = TRUE;
        top = x;
        maze[y][x] |= NORTH;
        carvesMade = totalCarve + 1;
    }
}

/**************************************************************************************************/
/* x and y are array coordinates from 1 to the maze's width/height, dir is the direction the maze */
/* carving is coming from (not the direction it's going - so a maze going to the east comes from  */
/* the west), and the waypoint refers to the branch coming from a given direction out of the      */
/* waypoint, with 1 being the path coming from the north of the waypoint, 2 from the east, 3 from */
/* the south, and 4 from the west.                                                                */
/*                                                                                                */
/* If so desired, the maze - complete or not - can be printed before anything else in the function*/
/* occurs. Then a bit of error checking occurs if the waypoint alley length is 0, to ensure no    */
/* dead ends are made where the maze carves one direction at the waypoint and then immediately    */
/* runs into an occupied space, leaving a tiny little path to nowhere. After that the bookkeeping */
/* is done, and then a few major checks are done. If the number of spaces carved is large enough, */
/* the maze starts carving from a new location by looping through the other branches and carving  */
/* from the end of their path if possible. Then the maze checks if the straightPercentage value is*/
/* larger than 0 - if it is, the maze continues in its current direction roughly that percentage  */
/* of the time. Otherwise, the maze chooses a random direction. Finally, the maze is printed if   */
/* it's returned all the way to the original alley.                                               */
/**************************************************************************************************/ 

void carveMaze(int x, int y, int dir, char wayPoint)
{
    if (x == xWayPoint && y == yWayPoint)
    {
        switch(dir)
        {
            case NORTH: if (maze[y + 1][x] != NO_DIRECTIONS) return;
            case EAST:  if (maze[y][x - 1] != NO_DIRECTIONS) return;
            case SOUTH: if (maze[y - 1][x] != NO_DIRECTIONS) return;
            case WEST:  if (maze[y][x + 1] != NO_DIRECTIONS) return;
        }
    }

    carvesMade++;
    switch(wayPoint)
    {
        case 1: w1.x = x; w1.y = y; w1.direction = dir; break;
        case 2: w2.x = x; w2.y = y; w2.direction = dir; break;
        case 3: w3.x = x; w3.y = y; w3.direction = dir; break;
        case 4: w4.x = x; w4.y = y; w4.direction = dir; break;
    }

    switch(dir)
    {
        case NORTH:
            if (x != xWayPoint || y != yWayPoint) maze[y][x] |= dir;
            if (y == mazeHeight - 2) ifBottom(x, y);
            break;
        case EAST:
            if (x != xWayPoint || y != yWayPoint) maze[y][x] |= dir;
            if (y == mazeHeight - 2) ifBottom(x, y);
            else if (y == 1) ifTop(x, y);
            break;
        case WEST:
            if (x != xWayPoint || y != yWayPoint) maze[y][x] |= dir;
            if (y == mazeHeight - 2) ifBottom(x, y);
            else if (y == 1) ifTop(x, y);
            break;
        case SOUTH:
            if (x != xWayPoint || y != yWayPoint) maze[y][x] |= dir;
            if (y == 1) ifTop(x, y);
            break;
    }

    if (carvesMade >= totalCarve + 1) //If enough is carved or an end was reached
    {
        if (printAtStep) mazePrint();
        carvesMade = 0;
        char wayPointIndices[TOTAL_DIRECTIONS] = {1, 2, 3, 4}; //char to save memory, even if not much
        shuffle(wayPointIndices);
        for (int i = 0; i < TOTAL_DIRECTIONS; i++) //Loop through each branch and carve from there
        {
            if (wayPointIndices[i] != wayPoint)
            {
                switch(wayPointIndices[i])
                {
                    case 1: newLocation(&w1, 1); break;
                    case 2: newLocation(&w2, 2); break;
                    case 3: newLocation(&w3, 3); break;
                    case 4: newLocation(&w4, 4); break;
                }
            }
        }
    }
    
    int random = rand() % 100;
    if (straightPercentage > random)
    {
        switch(wayPoint)
        {
            case 1: determinePath(&w1, 1); break;
            case 2: determinePath(&w2, 2); break;
            case 3: determinePath(&w3, 3); break;
            case 4: determinePath(&w4, 4); break;
        }
    }    

    char possibleDirections[TOTAL_DIRECTIONS] = {NORTH, EAST, SOUTH, WEST};
    shuffle(possibleDirections);
    for (int i = 0; i < TOTAL_DIRECTIONS; i++)
    {
        if (possibleDirections[i] == NORTH && maze[y - 1][x] == NO_DIRECTIONS)
        {
            maze[y][x] |= NORTH;
            carveMaze(x, y - 1, SOUTH, wayPoint);
        }
        else if (possibleDirections[i] == EAST && maze[y][x + 1] == NO_DIRECTIONS)
        {
            maze[y][x] |= EAST;
            carveMaze(x + 1, y, WEST, wayPoint);
        }
        else if (possibleDirections[i] == SOUTH && maze[y + 1][x] == NO_DIRECTIONS)
        {
            maze[y][x] |= SOUTH;
            carveMaze(x, y + 1, NORTH, wayPoint);
        }
        else if (possibleDirections[i] == WEST && maze[y][x - 1] == NO_DIRECTIONS)
        {
            maze[y][x] |= WEST;
            carveMaze(x - 1, y, EAST, wayPoint);
        }
    }
    
    if (printAtStep)
    {
        switch(wayPoint)
        {
            case 1: if (x == w1.xStart && y == w1.yStart) mazePrint(); break;
            case 2: if (x == w2.xStart && y == w2.yStart) mazePrint(); break;
            case 3: if (x == w3.xStart && y == w3.yStart) mazePrint(); break;
            case 4: if (x == w4.xStart && y == w4.yStart) mazePrint(); break;
        }
    }
}

/**************************************************************************************************/
/* Called only when the maze is to start carving from a different branch. If that branch is not at*/
/* the waypoint start (the very middle, not the end of the alley), then the maze starts carving   */
/* from the same cell it ended at before, or where the alley ended. Otherwise, the branch starts  */
/* at the waypoint itself and the alley length is 0, so exta handling to ensure it starts from the*/
/* middle of the waypoint is needed. If the adjacent cell is open, carve out the opposite         */
/* direction in the current cell (so if the direction is NORTH, carve out the south side of the   */
/* current cell) and tell the adjacent cell the maze is carving from that direction.              */
/**************************************************************************************************/

void newLocation(struct WayPointEnd *w, int wayPoint)
{
    if ((*w).x != xWayPoint || (*w).y != yWayPoint) carveMaze((*w).x, (*w).y, (*w).direction, wayPoint);

    else
    {
        switch((*w).direction)
        {
            case NORTH: if (maze[(*w).y + 1][(*w).x] == NO_DIRECTIONS)
                        {
                            maze[(*w).y][(*w).x] |= SOUTH;
                            carveMaze((*w).x, (*w).y + 1, NORTH, wayPoint);
                        }
                        break;
            case EAST:  if (maze[(*w).y][(*w).x - 1] == NO_DIRECTIONS)
                        {
                            maze[(*w).y][(*w).x] |= WEST;
                            carveMaze((*w).x - 1, (*w).y, EAST, wayPoint);
                        }
            case SOUTH: if (maze[(*w).y - 1][(*w).x] == NO_DIRECTIONS)
                        {
                            maze[(*w).y][(*w).x] |= NORTH;
                            carveMaze((*w).x, (*w).y - 1, SOUTH, wayPoint);
                        }
            case WEST:  if (maze[(*w).y][(*w).x + 1] == NO_DIRECTIONS)
                        {
                            maze[(*w).y][(*w).x] |= EAST;
                            carveMaze((*w).x + 1, (*w).y, WEST, wayPoint);
                        }
        }
    }
}

/**************************************************************************************************/
/* Called only when the maze needs to continue straight. The WayPointEnd struct w refers to one of*/
/* the four waypoint structures keeping track of each branch from the waypoint, and waypoint is   */
/* the same as described in carveMaze's function comments. The function uses the point's current  */
/* direction and adjusts the current space's properties to keep going straight, and calls         */
/* carveMaze with the necessary parameters to continue in the previous direction.                 */
/**************************************************************************************************/

void determinePath(struct WayPointEnd *w, int wayPoint)
{
    switch((*w).direction)
    {
        case NORTH: if (maze[(*w).y + 1][(*w).x] == NO_DIRECTIONS)
                    {
                        maze[(*w).y][(*w).x] |= SOUTH;
                        carveMaze((*w).x, (*w).y + 1, NORTH, wayPoint);
                    }
                    break;
        case EAST:  if (maze[(*w).y][(*w).x - 1] == NO_DIRECTIONS)
                    {
                        maze[(*w).y][(*w).x] |= WEST;
                        carveMaze((*w).x - 1, (*w).y, EAST, wayPoint);
                    }
                    break;
        case SOUTH: if (maze[(*w).y - 1][(*w).x] == NO_DIRECTIONS)
                    {
                        maze[(*w).y][(*w).x] |= NORTH;
                        carveMaze((*w).x, (*w).y - 1, SOUTH, wayPoint);
                    }
                    break;
        case WEST:  if (maze[(*w).y][(*w).x + 1] == NO_DIRECTIONS)
                    {
                        maze[(*w).y][(*w).x] |= EAST;
                        carveMaze((*w).x + 1, (*w).y, WEST, wayPoint);
                    }
                    break;
    }
}

/***************************************************************************************************/
/* Shuffles the array using the Fisher-Yates shuffle algorithm, as used in the card hand lab. Takes*/
/* an array to be shuffled as a parameter and has no return value. Chooses a random number between */
/* 0 and the number of elements in the array and swaps it with the last element, then chooses a    */
/* random number between 0 and the number of elements - 1 and swaps it with the second to last     */
/* element, and so on. The function has no return value.                                           */
/***************************************************************************************************/

void shuffle(char arr[])
{
    int i, j, temp;
    for (i = 3; i > 0; i--)
    {
        j = rand() % (i + 1);
        temp = arr[j];
        arr[j] = arr[i];
        arr[i] = temp;
    }
}

/***************************************************************************************************/
/* Takes no parameters and has no return value - only prints out the maze. Prints out row by row.  */
/* Sets color to white, green or red depending on if the solver has run and marked a location as   */
/* the solution through the maze (green) and if it's an alley that's not part of the solution      */
/* (red), then resets the color to white after each character is printed.                          */
/***************************************************************************************************/

void mazePrint()
{
    for (int i = 1; i < mazeHeight - 1; i++)
    {
        for (int j = 1; j < mazeWidth - 1; j++)
        {
            if ((maze[i][j] & VISITED) > 0) printf("\033[22;32m");
            else if ((maze[i][j] & SPECIAL) > 0) printf("\033[22;31m");
            else printf("\033[01;37m");
            switch(maze[i][j] & ALL_DIRECTIONS)
            {
                case 0: printf("%c", 219); break;
                case 1: printf("%c", 208); break;
                case 2: printf("%c", 198); break;
                case 3: printf("%c", 200); break;
                case 4: printf("%c", 210); break;
                case 5: printf("%c", 186); break;
                case 6: printf("%c", 201); break;
                case 7: printf("%c", 204); break;
                case 8: printf("%c", 181); break;
                case 9: printf("%c", 188); break;
                case 10: printf("%c", 205); break;
                case 11: printf("%c", 202); break;
                case 12: printf("%c", 187); break;
                case 13: printf("%c", 185); break;
                case 14: printf("%c", 203); break;
                case 15: printf("%c", 206); break;
            }
            printf("\033[01;37m");
        }
        printf("\n");
    }
    printf("\n");
}

/**************************************************************************************************/
/* Has no parameters or return value - does nothing aside from calling the recursive function     */
/* solutionSearch from the start of the maze to solve the maze.                                   */
/**************************************************************************************************/

void mazeSolve()
{
    solutionSearch(top, 1, NORTH);
}

/**************************************************************************************************/
/* Similar to carveMaze - takes x and y coordinates and the direction the searcher came from and  */
/* returns no value. The algorithm is as follows: The solver starts at the start (the initial call*/
/* to solutionSearch), then searches each direction to see if the maze goes in that direction.    */
/* Each spot in the maze visited by the solver is marked as part of the solution until all        */
/* directions have been searched from it and none lead to the end. When the solver reaches the end*/
/*  of the maze, the isFound flag is set to 1 and thus the function will do no more as all the    */
/* !isFound conditions will fail. At that point, each level of recursion simply returns and the   */
/* only highlighted parts of the maze will be the ones along the solution path.                   */
/**************************************************************************************************/

void solutionSearch(int x, int y, int dir)
{
    maze[y][x] |= VISITED;
    if (y == mazeHeight - 2 && x == bottom)
    {
        ifFound = TRUE;
        return;
    }
    for (int i = 0; i < TOTAL_DIRECTIONS; i++)
    {
        if (DIRECTION_LIST[i] == dir) continue; //Speed up process
        else if (DIRECTION_LIST[i] == NORTH && ((maze[y][x] & NORTH) == NORTH))
        {
            solutionSearch(x, y - 1, SOUTH);
        }
        else if (DIRECTION_LIST[i] == EAST && ((maze[y][x] & EAST) == EAST) && !ifFound)
        {
            solutionSearch(x + 1, y, WEST);
        }
        else if (DIRECTION_LIST[i] == SOUTH && ((maze[y][x] & SOUTH) == SOUTH) && !ifFound)
        {
            solutionSearch(x, y + 1, NORTH);
        }
        else if (DIRECTION_LIST[i] == WEST && ((maze[y][x] & WEST) == WEST) && !ifFound)
        {
            solutionSearch(x - 1, y, EAST);
        }
        if (ifFound) break;
    }
    if (ifFound == FALSE)
    {
        maze[y][x] -= VISITED;
    }
}

/**************************************************************************************************/
/* Frees the maze's dynamically allocated memory, so takes no parameters and returns no value. If */
/* the global variable hasBeenFreed is 0 (indicating that there is memory that hasn't been freed),*/
/* then the two dimensions of the array are freed, and hasBeenFreed is set to 1 to indicate the   */
/* memory was freed so that if mazeFree is called again there is no double freeing.               */
/**************************************************************************************************/

void mazeFree()
{
    if (!hasBeenFreed)
    {
        for (int i = 0; i < mazeHeight; i++)
        {
            free(maze[i]);
        }
        free(maze);
        hasBeenFreed = 1;
    }
}

/**************************************************************************************************/
/* Copies 4 bytes from an int to an unsigned char array where the least significant byte of the   */
/* int is placed in the first element of the array. Unchanged from sample instructor code.        */
/**************************************************************************************************/

void copyIntToAddress(int n, unsigned char bytes[])
{
  bytes[0] = n & 0xFF;
  bytes[1] = (n >>  8) & 0xFF;
  bytes[2] = (n >> 16) & 0xFF;
  bytes[3] = (n >> 24) & 0xFF;
}

/**************************************************************************************************/
/* Sets the RGB value of a single pixel at coordinates (x, y) in the character array data in      */
/* bitmap format with three bytes per pixel. Unchanged from sample instructor code.               */
/**************************************************************************************************/

void setRGB(unsigned char data[], int x, int y, int rowSize, int pixelHeight,
            unsigned char r, unsigned char g, unsigned char b)
{
    y = (pixelHeight - y) - 1;
    int offset = (x * 3) + (y * rowSize);
    data[offset] = b;
    data[offset+1] = g;
    data[offset+2] = r;
}

/**************************************************************************************************/
/* Takes no parameters and returns no value - simply creates the bitmap image with the maze tiles.*/
/* the height and width of the image are designed to be the maze's height multiplied by the       */
/* number of pixels in the bitmap image - this function only works for images 12 pixels x 12      */
/* pixels, which requires no row padding. Much of the first half of the function is otherwise     */
/* similar to BMP_ImageWriter.c. Then it writes each tile to the image similar to how mazePrint   */
/* prints out characters - with a nested for loop to iterate over each cell of the maze and place */
/* the corresponding tile there. With all that info, the image is made and outputted to maze.bmp. */
/**************************************************************************************************/

void makeImage()
{
    //Sample instructor code with a couple modifications for the specific pixel height/width
    int pixelWidth = (mazeWidth - 2) * 12;
    int pixelHeight = (mazeHeight - 2) * 12;
    int rowSize = pixelWidth * 3;
    int rowPadding = 0;
    rowPadding = (4 - (rowSize % 4)) % 4;
    rowSize += rowPadding;
    int pixelDataSize = rowSize*pixelHeight;
    int fileSize = 54 + pixelDataSize;

    //printf("rowPadding=%d bytes\n", rowPadding);
    //printf("rowSize   =%d bytes\n", rowSize);

    //Instructor sample code for the header unchanged
    unsigned char header[54] = 
    {
        'B','M',  // magic number
        0,0,0,0,  // size in bytes (set below)
        0,0,0,0,  // reserved
        54,0,0,0, // offset to start of pixel data
        40,0,0,0, // info hd size
        0,0,0,0,  // image width (set below)
        0,0,0,0,  // image heigth (set below)
        1,0,      // number color planes
        24,0,     // bits per pixel
        0,0,0,0,  // compression is none
        0,0,0,0,  // image bits size
        0x13,0x0B,0,0, // horz resoluition in pixel / m
        0x13,0x0B,0,0, // vert resolutions (0x03C3 = 96 dpi, 0x0B13 = 72 dpi)
        0,0,0,0,  // #colors in pallete
        0,0,0,0,  // #important colors
    };

    //Sample instructor code
    copyIntToAddress(fileSize, &header[2]);
    copyIntToAddress(pixelWidth, &header[18]);
    copyIntToAddress(pixelHeight, &header[22]);
    copyIntToAddress(pixelDataSize, &header[34]);

    unsigned char img[pixelDataSize];

    //Initialize all pixels to black. This also sets any row padding
    //  to 0FF, but that data is ignored by readers.
    memset(img,0x00,sizeof(img));

    for (int i = 1; i < mazeHeight - 1; i++)
    {
        for (int j = 1; j < mazeWidth - 1; j++)
        {
            switch(maze[i][j] & ALL_DIRECTIONS)
            {
                //Note: because of how data is read out of these images, the tiles are mirror images
                //so each east must be changed to a west and vice versa if the tiles are not vertically
                //symmetric
                case 1: processImage(img, j, i, rowSize, pixelHeight, "North.bmp"); break;
                case 2: processImage(img, j, i, rowSize, pixelHeight, "West.bmp"); break;
                case 3: processImage(img, j, i, rowSize, pixelHeight, "North-west.bmp"); break;
                case 4: processImage(img, j, i, rowSize, pixelHeight, "South.bmp"); break;
                case 5: processImage(img, j, i, rowSize, pixelHeight, "North-south.bmp"); break;
                case 6: processImage(img, j, i, rowSize, pixelHeight, "South-west.bmp"); break;
                case 7: processImage(img, j, i, rowSize, pixelHeight, "South-west-north.bmp"); break;
                case 8: processImage(img, j, i, rowSize, pixelHeight, "East.bmp"); break;
                case 9: processImage(img, j, i, rowSize, pixelHeight, "North-east.bmp"); break;
                case 10: processImage(img, j, i, rowSize, pixelHeight, "East-west.bmp"); break;
                case 11: processImage(img, j, i, rowSize, pixelHeight, "West-north-east.bmp"); break;
                case 12: processImage(img, j, i, rowSize, pixelHeight, "South-east.bmp"); break;
                case 13: processImage(img, j, i, rowSize, pixelHeight, "South-east-north.bmp"); break;
                case 14: processImage(img, j, i, rowSize, pixelHeight, "South-west-east.bmp"); break;
                case 15: processImage(img, j, i, rowSize, pixelHeight, "All-directions.bmp"); break;
            }
        }
    }
    FILE* imageFile = fopen("maze.bmp", "wb");
    fwrite(header, 1, sizeof(header), imageFile);
    fwrite(img, 1, sizeof(img), imageFile);
    fclose(imageFile);
}

/**************************************************************************************************/
/* Parameters: img[] is the array with the image information, int x and y are the coordinates of  */
/* the cell in the maze, and rowSize and pixelHeight are used only to be used in the call to the  */
/* setRGB function. It opens the tile image to be read in binary, then saves the RGB data in a 2D */
/* array (skipping over the first 54 bytes, which represent the header). Then it sends that RGB   */
/* data to the setRGB function in the spot on the larger image the tile is on.                    */
/**************************************************************************************************/

void processImage(unsigned char img[], int x, int y, int rowSize, int pixelHeight, char name[])
{
    FILE *tile = fopen(name, "rb");
    if (tile == NULL)
    {
        printf("nope\n");
        return;
    }
    unsigned char data[144][3];
    unsigned char byte;
    for (int i = 0; i < 54; i++) byte = getc(tile);
    for (int i = 0; i < 144; i++)
    {
        data[i][2] = getc(tile);
        data[i][1] = getc(tile);
        data[i][0] = getc(tile);
    }

    int count = 0;
    for (int i = (y - 1) * 12 + 11; i > (y - 1) * 12 - 1; i--)
    {
        for (int j = (x - 1) * 12 + 11; j > (x - 1) * 12 - 1; j--)
        {
            setRGB(img, j, i, rowSize, pixelHeight, data[count][0], data[count][1], data[count][2]);
            count++;
        }
    }
}