/*****************************************************************************/
/* Author: Andrew Barndt                                                     */
/* Description: A maze generator that uses recursion to create random mazes  */
/* with a built-in solver. Dynamically allocates memory to ensure no memory  */
/* leaks are possible, and frees the memory used for the maze after each is  */
/* generated.                                                                */
/*****************************************************************************/ 

#include "mazegen.h"
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

//function prototypes
void carveMaze(int x, int y, int dir);
void shuffle(char *arr);
void solutionSearch(int x, int y, int dir);

static unsigned char **maze;
int hitBottom; //to keep track of if the maze has already reached the bottom to determine if the maze should
               //blast through the bottom or not
int ifFound; //used by the solver to keep track of if the way through has been found
int hasBeenFreed = 0; //keeps track of memory freeing
int mazeHeight, mazeWidth, start, bottom;

/*****************************************************************************/
/* width and height: can take values from 3 to 1000 to set the maze's        */
/* dimensions.                                                               */
/* All other parameters are not used for Milestone 1 of the project.         */
/* Starts by freeing the previous maze, then performs error checking. If all */
/* is good after the error checking, it then allocates memory for the maze   */
/* according to the width and height, and adds 2 to each so that the maze has*/
/* a "boundary" to eliminate out of bounds errors. It then randomly selects a*/
/* starting point to the maze, and calls carveMaze to do the rest.           */
/* Returns TRUE if an error is found, and otherwise returns FALSE.           */
/*****************************************************************************/

int mazeGenerate(int width, int height, int wayPointX, int wayPointY, int wayPointAlleyLength,
    double wayPointDirectionPercent,  double straightProbability, int printAlgorithmSteps)
{
    mazeFree();
    if (height < 3 || height > 1000 || width < 3 || width > 1000)
    {
        printf("Invalid dimensions\n");
        return TRUE;
    }
    if (wayPointX < 1 || wayPointX > width || wayPointY < 1 || wayPointY > height)
    {
        printf("Invalid waypoints\n");
        return TRUE;
    }
    if (((width > height) && (wayPointAlleyLength > height/2)) || wayPointAlleyLength < 0 ||
        ((height > width) && (wayPointAlleyLength > width/2)))
    {
        printf("Invalid alley length\n");
        return TRUE;
    }
    if (wayPointDirectionPercent < 0.0 || wayPointDirectionPercent > 1.0)
    {
        printf("Invalid percentages\n");
        return TRUE;
    }
    if (straightProbability < 0.0 || straightProbability > 1.0)
    {
        printf("Invalid probability\n");
        return TRUE;
    }
    if (printAlgorithmSteps > 1 || printAlgorithmSteps < 0)
    {
        printf("Invalid algorithm step input\n");
        return TRUE;
    }
    mazeHeight = height + 2; mazeWidth = width + 2;
    hitBottom = FALSE; ifFound = FALSE;

    maze = malloc(mazeHeight * sizeof(char *));
    for (int i = 0; i < mazeHeight; i++)
    { maze[i] = malloc(mazeWidth * sizeof(char));
    }

    for (int i = 1; i < mazeWidth - 1; i++)
    {
        for (int j = 1; j < mazeHeight - 1; j++)
        {
            maze[j][i] = 0;
        }
    }

    hasBeenFreed = 0;
    //Give the boundary of the maze values of 15 to ensure they're never blasted into
    for (int i = 0; i < mazeWidth; i++) maze[0][i] = ALL_DIRECTIONS;
    for (int i = 0; i < mazeWidth; i++) maze[mazeHeight - 1][i] = ALL_DIRECTIONS;
    for (int i = 0; i < mazeHeight; i++) maze[i][0] = ALL_DIRECTIONS;
    for (int i = 0; i < mazeHeight; i++) maze[i][mazeWidth - 1] = ALL_DIRECTIONS;
    start = (rand() % width) + 1;
   
    carveMaze(start, 1, NORTH);
    return FALSE;
}

/*****************************************************************************/
/* x and y are the currect array coordinates, which can be anywhere from 1 to*/
/* width - 1 or height - 1. dir is the direction the call to carveMaze came  */
/* from, which can be any of the four directions north, east, south, or west.*/
/* Each point on the maze could possibly go four directions, so an array of  */
/* those directions is created and then randomly shuffled to keep the maze   */
/* choosing random directions. The function then uses the direction given to */
/* it to adjust the current spot of the maze, and then chooses the next      */
/* direction in the randomly shuffled array to see if it's open. If it is,   */
/* the current array coordinates are modified accordingly and carveMaze is   */
/* recursively called. If not, then the loop keeps running until it finds an */
/* open direction or searches through all 4 directions, at which point the   */
/* function is finished and returns one lower level of recursion.            */
/*****************************************************************************/ 

void carveMaze(int x, int y, int dir)
{
    //array gets destroyed when function returns anyway so no need to dynamically allocate
    char possibleDirections[TOTAL_DIRECTIONS] = {NORTH, EAST, SOUTH, WEST};
    shuffle(possibleDirections);
    switch(dir)
    {
        case NORTH:
            maze[y][x] = dir;
            if(hitBottom == FALSE && y == mazeHeight - 2) //First time it hits the bottom of the maze is the solution
            {           
                hitBottom = TRUE;
                maze[y][x] = (maze[y][x] | SOUTH);
                bottom = x;
                return; //Makes the part that goes out of the bottom always a long vertical pipe
            }
            break;
        case EAST: 
            maze[y][x] = dir;
            break;
        case SOUTH:
            maze[y][x] = dir;
            break;
        case WEST:
            maze[y][x] = dir;
            break;
    }
    
    for (int i = 0; i < TOTAL_DIRECTIONS; i++)
    {
        if (possibleDirections[i] == NORTH)
        {
            if (maze[y - 1][x] == NO_DIRECTIONS)
            {
                maze[y][x] = (maze[y][x] | NORTH);
                carveMaze(x, y - 1, SOUTH);
            }
        }
        else if (possibleDirections[i] == EAST)
        {
            if (maze[y][x + 1] == NO_DIRECTIONS)
            {
                maze[y][x] = (maze[y][x] | EAST);
                carveMaze(x + 1, y, WEST);
            }
        }
        else if (possibleDirections[i] == SOUTH)
        {
            if (maze[y + 1][x] == NO_DIRECTIONS)
            {
                maze[y][x] = (maze[y][x] | SOUTH);
                carveMaze(x, y + 1, NORTH);
            }
        }
        else if (possibleDirections[i] == WEST)
        {
            if (maze[y][x - 1] == NO_DIRECTIONS)
            {
                maze[y][x] = (maze[y][x] | WEST);
                carveMaze(x - 1, y, EAST);
            }
        } 
    }
}

/*****************************************************************************/
/* Shuffles the array using the Fisher-Yates shuffle algorithm, as used in   */
/* the card hand lab. Takes an array to be shuffled as a parameter and has no*/
/* return value. Chooses a random number between 0 and the number of elements*/
/* in the array and swaps it with the last element, then chooses a random    */
/* number between 0 and the number of elements - 1 and swaps it with the     */
/* second to last element, and so on. The function has no return value.      */
/*****************************************************************************/

void shuffle(char arr[])
{
    int i, j, temp;
    for (i = 3; i > 0; i--)
    {
        j = rand() % (i + 1);
        temp = arr[j];
        arr[j] = arr[i];
        arr[i] = temp;
    }
}

/*****************************************************************************/
/* Takes no parameters and has no return value - only prints out the maze.   */
/* Prints out row by row. Sets color to white or green depending on if the   */
/* solver has run and marked a location as the solution through the maze,    */
/* then resets the color to white after each character is printed.           */
/*****************************************************************************/

void mazePrint()
{
    for (int i = 1; i < mazeHeight - 1; i++)
    {
        for (int j = 1; j < mazeWidth - 1; j++)
        {
            if (maze[i][j] > ALL_DIRECTIONS) printf("\033[22;32m");
            else printf("\033[01;37m");
            switch(maze[i][j] & ALL_DIRECTIONS)
            {
                case 0: printf("%c", 219); break;
                case 1: printf("%c", 208); break;
                case 2: printf("%c", 198); break;
                case 3: printf("%c", 200); break;
                case 4: printf("%c", 210); break;
                case 5: printf("%c", 186); break;
                case 6: printf("%c", 201); break;
                case 7: printf("%c", 204); break;
                case 8: printf("%c", 181); break;
                case 9: printf("%c", 188); break;
                case 10: printf("%c", 205); break;
                case 11: printf("%c", 202); break;
                case 12: printf("%c", 187); break;
                case 13: printf("%c", 185); break;
                case 14: printf("%c", 203); break;
                case 15: printf("%c", 206); break;
            }
            printf("\033[01;37m");
        }
        printf("\n");
    }
    printf("\n");
}

/*****************************************************************************/
/* Has no parameters or return value - does nothing aside from calling the   */
/* recursive function solutionSearch from the start of the maze to solve the */
/* maze.                                                                     */
/*****************************************************************************/

void mazeSolve()
{
    solutionSearch(start, 1, NORTH);
}

/*****************************************************************************/
/* Similar to carveMaze - takes x and y coordinates and the direction the    */
/* searcher came from and returns no value. The algorithm is as follows: The */
/* solver starts at the start (the initial call to solutionSearch), then     */
/* searches each direction to see if the maze goes in that direction. Each   */
/* spot in the maze visited by the solver is marked as part of the solution  */
/* until all directions have been searched from it and none lead to the end. */
/* When the solver reaches the end of the maze, the isFound flag is set to 1 */
/* and thus the function will do no more as all the !isFound conditions will */
/* fail. At that point, each level of recursion simply returns and the only  */
/* highlighted parts of the maze will be the ones along the solution path.   */
/*****************************************************************************/

void solutionSearch(int x, int y, int dir)
{
    maze[y][x] = maze[y][x] | VISITED;
    if (y == mazeHeight - 2 && x == bottom)
    {
        ifFound = TRUE;
        return;
    }
    for (int i = 0; i < TOTAL_DIRECTIONS; i++)
    {
        if (DIRECTION_LIST[i] == dir) continue; //Speed up process
        else if (DIRECTION_LIST[i] == NORTH && ((maze[y][x] & NORTH) == NORTH))
        {
            solutionSearch(x, y - 1, SOUTH);
        }
        else if (DIRECTION_LIST[i] == EAST && ((maze[y][x] & EAST) == EAST) && !ifFound)
        {
            solutionSearch(x + 1, y, WEST);
        }
        else if (DIRECTION_LIST[i] == SOUTH && ((maze[y][x] & SOUTH) == SOUTH) && !ifFound)
        {
            solutionSearch(x, y + 1, NORTH);
        }
        else if (DIRECTION_LIST[i] == WEST && ((maze[y][x] & WEST) == WEST) && !ifFound)
        {
            solutionSearch(x - 1, y, EAST);
        }
        if (ifFound) break;
    }
    if (ifFound == FALSE)
    {
        maze[y][x] = (maze[y][x] & ALL_DIRECTIONS);
    }
}

/*****************************************************************************/
/* Frees the maze's dynamically allocated memory, so takes no parameters and */
/* returns no value. If the global variable hasBeenFreed is 0 (indicating    */
/* that there is memory that hasn't been freed), then the two dimensions of  */
/* the array are freed, and hasBeenFreed is set to 1 to indicate the memory  */
/* was freed so that if mazeFree is called again there is no double freeing. */
/*****************************************************************************/

void mazeFree()
{
    if (!hasBeenFreed)
    {
        for (int i = 0; i < mazeHeight; i++)
        {
            free(maze[i]);
        }
        free(maze);
        hasBeenFreed = 1;
    }
}