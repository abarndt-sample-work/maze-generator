**Overview**
<p>The code in this repository is used to generate randomly generated mazes using pipe characters.</p>

**Specifications/Concepts**
<p>The idea was to use recursion and pseudorandom numbers in a 2D array to create a maze with one (and only one) path through.</p>
<p>MazeGeneratorRandom.c simply starts at the top of the maze and works its way down until it finds somewhere at the bottom, then recursively fills in the rest of the maze.</p>
<p>MazeGeneratorWithWaypoint.c uses a few more concepts: a waypoint (a particular location in the maze that the solution is supposed to go through) and sometimes a corresponding alley (branches going straight out from the waypoint, resulting in a cross shape) and straight probability (the likelihood the maze goes straight - a value of 1 guarantees the maze carves straight until it physically can't, and 0 means a random direction), and waypoint direction percent (the amount the maze generates from one of the waypont branches before making the maze at another branch). It also prints out the algorithm steps, to see the maze being generated at certain points before completion.</p>
<p>MazeGeneratorWaypointBitmap.c keeps the code from MazeGeneratorWithWaypoint.c, but produces a bitmap image of the maze when run made from "tiles" of 12 x 12 bitmap images for each maze cell possibility.</p>
<p>mazegen.h is the header file the instructor for the class wrote, containing several important fields and function prototypes.</p>
<p>Mazetest.c simply calls the function to generate the mazes. Part 1 can be used for both MazeGeneratorRandom.c or MazeGeneratorWithWayPoint.c, but parts 2-4 were designed only for MazeGeneratorRandom.c.</p>

**Running the Program**
<p>Put Mazetest.c, mazegen.h, and one of the three generator files in an empty directory on a Linux machine (only one of MazeGeneratorRandom.c, MazeGeneratorWithWaypoint.c, or MazeGeneratorWaypointBitmap.c may be in the directory at the same time), using the character set CP866 to print the pipe characters. Compile with gcc *.c and run with ./a.out.</p>
<p>Note: for running MazeGeneratorWaypointBitmap, the images in the maze-tiles folder will need to be in the same directory as the rest of the code (outside of their own folder - the directory will have the code, header file, and 15 tile images), or else the bitmap maze will not be created.</p>